// name	  : background.js
// author : Simon Descarpentres
// date   : 2017-06, 2020-10
// licence: GPLv3
/* globals browser */
/* Open a new tab, and load "index.html" in it. */

function openMetaPress() {
	console.log('injecting meta-press.es')
	browser.tabs.create({
		'url': '/index.html'
	})
}
browser.browserAction.onClicked.addListener(openMetaPress)
/* Announces updates : opens the official link when new version is installed */
async function announce_updates () {
	let manifest = await fetch('manifest.json')
	manifest = await manifest.json()
	let stored_version = await localStorage.persist().getItem('version')
	if (stored_version != manifest.version) {
		browser.tabs.create({
			'url': 'https://www.meta-press.es/category/journal.html'
		})
		localStorage.persist().setItem('version', manifest.version)
	}
}
var announce_update = true
if (announce_update)
	announce_updates()
